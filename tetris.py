# Audio: http://soundbible.com/1086-Wooden-Thump.html edited by Tim Hornback

import arcade
import os
import random
import easygui

PIECE_WIDTH = 64
SPRITE_SCALING = 0.5
# You can change row/column count to increase game size without breaking everything
COLUMN_COUNT = 10
ROW_COUNT = 20

SIZE = PIECE_WIDTH * SPRITE_SCALING
GAME_WIDTH = int(PIECE_WIDTH * SPRITE_SCALING * COLUMN_COUNT)
SCREEN_WIDTH = GAME_WIDTH + 200
SCREEN_HEIGHT = int(PIECE_WIDTH * SPRITE_SCALING * ROW_COUNT)
RIGHT_INFO = SCREEN_WIDTH - (SCREEN_WIDTH - GAME_WIDTH) // 2

file_path = os.path.dirname(os.path.abspath(__file__))
os.chdir(file_path)

cyan = "tetromino_cyan.png"
blue = "tetromino_blue.png"
orange = "tetromino_orange.png"
yellow = "tetromino_yellow.png"
green = "tetromino_green.png"
purple = "tetromino_purple.png"
red = "tetromino_red.png"
cyan_full = "tetromino_cyan_full.png"
blue_full = "tetromino_blue_full.png"
orange_full = "tetromino_orange_full.png"
yellow_full = "tetromino_yellow_full.png"
green_full = "tetromino_green_full.png"
purple_full = "tetromino_purple_full.png"
red_full = "tetromino_red_full.png"
cyan_texture = arcade.load_texture(cyan)
blue_texture = arcade.load_texture(blue)
orange_texture = arcade.load_texture(orange)
yellow_texture = arcade.load_texture(yellow)
green_texture = arcade.load_texture(green)
purple_texture = arcade.load_texture(purple)
red_texture = arcade.load_texture(red)
cyan_texture_full = arcade.load_texture(cyan_full)
blue_texture_full = arcade.load_texture(blue_full)
orange_texture_full = arcade.load_texture(orange_full)
yellow_texture_full = arcade.load_texture(yellow_full)
green_texture_full = arcade.load_texture(green_full)
purple_texture_full = arcade.load_texture(purple_full)
red_texture_full = arcade.load_texture(red_full)
color_dict = {cyan: 1, blue: 2, orange: 3, yellow: 4, green: 5, purple: 6, red: 7}
texture_dict = {
    1: cyan_texture,
    2: blue_texture,
    3: orange_texture,
    4: yellow_texture,
    5: green_texture,
    6: purple_texture,
    7: red_texture,
    8: cyan_texture_full,
    9: blue_texture_full,
    10: orange_texture_full,
    11: yellow_texture_full,
    12: green_texture_full,
    13: purple_texture_full,
    14: red_texture_full,
}


class Tetromino(arcade.Sprite):
    """
    Tetromino object. Individual squares of each piece.
    """

    def __init__(self, file_name, sprite_scaling):

        # Initialize object and declare variables
        super().__init__(file_name, sprite_scaling)
        self.file_name = file_name
        self.is_moving = True
        self.frame_count = 0
        self.rotate_point_x = 0
        self.rotate_point_y = 0
        self.level = 0
        self.drop_speed = int(30 / (1 + (self.level * 0.1)))

    def update(self):

        # Currently runs every 1/60th of a second. Causes squares to fall faster the farther you get and resets change_x so the square only moves once per key press.
        self.frame_count += 1
        if self.is_moving:
            if self.frame_count % self.drop_speed == 0:
                self.center_y -= SIZE
                self.rotate_point_y -= SIZE
            self.center_x += self.change_x
            if self.change_x is not 0:
                self.rotate_point_x += self.change_x
                self.change_x = 0


class Tetris(arcade.Window):
    def __init__(self, width, height):
        """
        Initializer for game. Declare variables and initialize window.
        """
        super().__init__(width, height)

        file_path = os.path.dirname(os.path.abspath(__file__))
        os.chdir(file_path)
        self.level = 0
        self.game_over = True
        self.square_list = []
        self.piece_list = []
        self.piece_held = None
        self.next_piece = random.randrange(1, 8)
        self.frame_count = 0
        self.grid = []
        self.score = 0
        self.has_switched = False
        self.thud = arcade.load_sound("thud.ogg")
        self.line_cleared = arcade.load_sound("twoTone2.ogg")

    def setup(self):

        # Sprite lists
        self.square_list = arcade.SpriteList()
        self.piece_list = arcade.SpriteList()
        self.new_grid()

        # Set background color
        arcade.set_background_color(arcade.color.GRAY_BLUE)

    def on_draw(self):
        """
        Render the screen.
        """
        arcade.start_render()
        self.piece_list.draw()

        # Area to the right of play zone
        arcade.draw_lrtb_rectangle_filled(
            GAME_WIDTH, SCREEN_WIDTH, SCREEN_HEIGHT, 0, arcade.color.GRAY
        )
        arcade.draw_rectangle_filled(
            RIGHT_INFO, SCREEN_HEIGHT - 100, 170, 170, arcade.color.GRAY_BLUE
        )
        arcade.draw_rectangle_filled(
            RIGHT_INFO, SCREEN_HEIGHT - 320, 170, 170, arcade.color.GRAY_BLUE
        )
        arcade.draw_text(
            "Held",
            RIGHT_INFO,
            SCREEN_HEIGHT - 30,
            arcade.color.WHITE,
            14,
            align="center",
            anchor_x="center",
            anchor_y="center",
        )
        arcade.draw_text(
            "Next",
            RIGHT_INFO,
            SCREEN_HEIGHT - 250,
            arcade.color.WHITE,
            14,
            align="center",
            anchor_x="center",
            anchor_y="center",
        )

        if self.piece_held:
            hold_texture = texture_dict[self.piece_held + 7]
            arcade.draw_texture_rectangle(
                RIGHT_INFO,
                SCREEN_HEIGHT - 95,
                hold_texture.width / 2,
                hold_texture.height / 2,
                hold_texture,
            )

        if self.next_piece > 0:
            next_texture = texture_dict[self.next_piece + 7]
            arcade.draw_texture_rectangle(
                RIGHT_INFO,
                SCREEN_HEIGHT - 315,
                next_texture.width / 2,
                next_texture.height / 2,
                next_texture,
            )

        # Draws textures for corresponding values in self.grid to the correct textures.
        for row in range(ROW_COUNT):
            for column in range(COLUMN_COUNT):
                if self.grid[row][column]:
                    arcade.draw_texture_rectangle(
                        SIZE * column + (SIZE // 2),
                        SIZE * row + (SIZE // 2),
                        SIZE,
                        SIZE,
                        texture_dict[self.grid[row][column]],
                    )

        # Text rendering
        output = f"Score: {self.score}"
        arcade.draw_text(
            output,
            RIGHT_INFO,
            20,
            arcade.color.WHITE,
            14,
            align="center",
            anchor_x="center",
            anchor_y="center",
        )
        output = f"Level: {self.level}"
        arcade.draw_text(
            output,
            RIGHT_INFO,
            40,
            arcade.color.WHITE,
            14,
            align="center",
            anchor_x="center",
            anchor_y="center",
        )

        if self.game_over:
            # After at least one game is played, show this
            if self.frame_count:
                arcade.draw_lrtb_rectangle_filled(
                    GAME_WIDTH, SCREEN_WIDTH, SCREEN_HEIGHT, 100, arcade.color.GRAY
                )
                arcade.draw_text(
                    "Game Over",
                    RIGHT_INFO,
                    SCREEN_HEIGHT // 2 - 10,
                    arcade.color.WHITE,
                    30,
                    align="center",
                    anchor_x="center",
                    anchor_y="center",
                )
                arcade.draw_text(
                    f"Final Score: {self.score}.  If you would like to save your score, press Q.",
                    RIGHT_INFO,
                    SCREEN_HEIGHT // 2 - 150,
                    arcade.color.WHITE,
                    20,
                    width=150,
                    align="center",
                    anchor_x="center",
                    anchor_y="center",
                )
                hs = open("hs.txt", "r")
                contents = hs.readlines()[:10]
                index = 0
                for item in contents:
                    index += 1
                    arcade.draw_text(
                        f"{item}",
                        RIGHT_INFO,
                        SCREEN_HEIGHT - index * 30,
                        arcade.color.WHITE,
                        20,
                        align="center",
                        anchor_x="center",
                        anchor_y="center",
                    )

            # Opening Screen
            else:
                arcade.draw_lrtb_rectangle_filled(
                    0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, arcade.color.BLUE_GRAY
                )
                arcade.draw_text(
                    "Welcome to Tetris!\nControls\nLeft Arrow/A - Move left\nRight Arrow/D - Move right\nDown Arrow/S - Move down\nUp Arrow/W - Rotate Clockwise\nQ - Switch Hold Piece\n\nPress Spacebar to begin",
                    SCREEN_WIDTH // 2,
                    SCREEN_HEIGHT // 2,
                    arcade.color.WHITE,
                    25,
                    align="center",
                    anchor_x="center",
                    anchor_y="center",
                )

    def new_grid(self):
        self.grid = []
        for row in range(ROW_COUNT):
            self.grid.append([])
            for column in range(COLUMN_COUNT):
                self.grid[row].append(0)

    def check_clear_row(self):
        """
        Make sure that the oldest piece has been completely transformed from sprite to grid,
        then check for full rows (top to bottom) and add score.
        """
        rows_popped = 0
        old_resolved = False
        for piece in self.piece_list:
            if piece.frame_count < 30:
                old_resolved = True
            else:
                old_resolved = False
                break
        if old_resolved:
            for row in range(ROW_COUNT - 1, -1, -1):
                row_sum = 0
                for column in range(COLUMN_COUNT - 1, -1, -1):
                    if self.grid[row][column] != 0:
                        row_sum += 1
                if row_sum == COLUMN_COUNT:
                    self.grid.pop(row)
                    rows_popped += 1
                    self.grid.append([])
                    for column in range(COLUMN_COUNT):
                        self.grid[ROW_COUNT - 1].append(0)
            if rows_popped:
                self.score += 100 + (rows_popped - 1) * 125
                arcade.play_sound(self.line_cleared)
            self.level = self.score // 150

    def new_shape(self, next_piece=None):
        """
        Select a new piece shape to create.
        """
        if next_piece:
            piece_picked = next_piece
        else:
            piece_picked = self.next_piece
        # Cyan piece "I"
        if piece_picked == 1:
            for i in range(
                int(-SIZE * 1.5), int(SIZE * 1.5 + 1), int(SPRITE_SCALING * PIECE_WIDTH)
            ):
                piece = Tetromino(cyan, SPRITE_SCALING)
                piece.center_x = GAME_WIDTH // 2 + i
                piece.center_y = SCREEN_HEIGHT - SIZE // 2
                piece.rotate_point_x = GAME_WIDTH // 2
                piece.rotate_point_y = SCREEN_HEIGHT - SIZE
                self.piece_list.append(piece)

        # Blue piece "J"
        if piece_picked == 2:
            for i in range(
                int(-SIZE * 1.5), int(SIZE * 0.5 + 1), int(SPRITE_SCALING * PIECE_WIDTH)
            ):
                piece = Tetromino(blue, SPRITE_SCALING)
                piece.center_x = GAME_WIDTH // 2 + i
                piece.center_y = SCREEN_HEIGHT - SIZE // 2
                piece.rotate_point_x = GAME_WIDTH // 2
                piece.rotate_point_y = SCREEN_HEIGHT - SIZE
                self.piece_list.append(piece)
            piece = Tetromino(blue, SPRITE_SCALING)
            piece.center_x = GAME_WIDTH // 2 + SIZE / 2
            piece.center_y = SCREEN_HEIGHT - SIZE * 1.5
            piece.rotate_point_x = GAME_WIDTH // 2
            piece.rotate_point_y = SCREEN_HEIGHT - SIZE
            self.piece_list.append(piece)

        # Orange piece "L"
        if piece_picked == 3:
            for i in range(
                int(-SIZE * 1.5), int(SIZE * 0.5 + 1), int(SPRITE_SCALING * PIECE_WIDTH)
            ):
                piece = Tetromino(orange, SPRITE_SCALING)
                piece.center_x = GAME_WIDTH // 2 + i
                piece.center_y = SCREEN_HEIGHT - SIZE // 2
                piece.rotate_point_x = GAME_WIDTH // 2
                piece.rotate_point_y = SCREEN_HEIGHT - SIZE
                self.piece_list.append(piece)
            piece = Tetromino(orange, SPRITE_SCALING)
            piece.center_x = GAME_WIDTH // 2 - SIZE * 1.5
            piece.center_y = SCREEN_HEIGHT - SIZE * 1.5
            piece.rotate_point_x = GAME_WIDTH // 2
            piece.rotate_point_y = SCREEN_HEIGHT - SIZE
            self.piece_list.append(piece)

        # Yellow piece "O"
        if piece_picked == 4:
            for i in range(
                int(-SIZE * 0.5), int(SIZE * 0.5 + 1), int(SPRITE_SCALING * PIECE_WIDTH)
            ):
                piece = Tetromino(yellow, SPRITE_SCALING)
                piece.center_x = GAME_WIDTH // 2 + i
                piece.center_y = SCREEN_HEIGHT - SIZE // 2
                piece.rotate_point_x = GAME_WIDTH // 2
                piece.rotate_point_y = SCREEN_HEIGHT - SIZE
                self.piece_list.append(piece)
            for i in range(
                int(-SIZE * 0.5), int(SIZE * 0.5 + 1), int(SPRITE_SCALING * PIECE_WIDTH)
            ):
                piece = Tetromino(yellow, SPRITE_SCALING)
                piece.center_x = GAME_WIDTH // 2 + i
                piece.center_y = SCREEN_HEIGHT - SIZE * 1.5
                piece.rotate_point_x = GAME_WIDTH // 2
                piece.rotate_point_y = SCREEN_HEIGHT - SIZE
                self.piece_list.append(piece)

        # Green piece "S"
        if piece_picked == 5:
            for i in range(
                int(-SIZE * 0.5), int(SIZE * 0.5 + 1), int(SPRITE_SCALING * PIECE_WIDTH)
            ):
                piece = Tetromino(green, SPRITE_SCALING)
                piece.center_x = GAME_WIDTH // 2 + i
                piece.center_y = SCREEN_HEIGHT - SIZE // 2
                piece.rotate_point_x = GAME_WIDTH // 2
                piece.rotate_point_y = SCREEN_HEIGHT - SIZE
                self.piece_list.append(piece)
            for i in range(
                int(-SIZE * 1.5),
                int(-SIZE * 0.5 + 1),
                int(SPRITE_SCALING * PIECE_WIDTH),
            ):
                piece = Tetromino(green, SPRITE_SCALING)
                piece.center_x = GAME_WIDTH // 2 + i
                piece.center_y = SCREEN_HEIGHT - SIZE * 1.5
                piece.rotate_point_x = GAME_WIDTH // 2
                piece.rotate_point_y = SCREEN_HEIGHT - SIZE
                self.piece_list.append(piece)

        # Purple piece "T"
        if piece_picked == 6:
            for i in range(
                int(-SIZE * 1.5), int(SIZE * 0.5 + 1), int(SPRITE_SCALING * PIECE_WIDTH)
            ):
                piece = Tetromino(purple, SPRITE_SCALING)
                piece.center_x = GAME_WIDTH // 2 + i
                piece.center_y = SCREEN_HEIGHT - SIZE // 2
                piece.rotate_point_x = GAME_WIDTH // 2
                piece.rotate_point_y = SCREEN_HEIGHT - SIZE
                self.piece_list.append(piece)
            piece = Tetromino(purple, SPRITE_SCALING)
            piece.center_x = GAME_WIDTH // 2 - SIZE / 2
            piece.center_y = SCREEN_HEIGHT - SIZE * 1.5
            piece.rotate_point_x = GAME_WIDTH // 2
            piece.rotate_point_y = SCREEN_HEIGHT - SIZE
            self.piece_list.append(piece)

        # Red Piece "Z"
        if piece_picked == 7:
            for i in range(
                int(-SIZE * 1.5),
                int(-SIZE * 0.5 + 1),
                int(SPRITE_SCALING * PIECE_WIDTH),
            ):
                piece = Tetromino(red, SPRITE_SCALING)
                piece.center_x = GAME_WIDTH // 2 + i
                piece.center_y = SCREEN_HEIGHT - SIZE // 2
                piece.rotate_point_x = GAME_WIDTH // 2
                piece.rotate_point_y = SCREEN_HEIGHT - SIZE
                self.piece_list.append(piece)
            for i in range(
                int(-SIZE * 0.5), int(SIZE * 0.5 + 1), int(SPRITE_SCALING * PIECE_WIDTH)
            ):
                piece = Tetromino(red, SPRITE_SCALING)
                piece.center_x = GAME_WIDTH // 2 + i
                piece.center_y = SCREEN_HEIGHT - SIZE * 1.5
                piece.rotate_point_x = GAME_WIDTH // 2
                piece.rotate_point_y = SCREEN_HEIGHT - SIZE
                self.piece_list.append(piece)
        if next_piece is None:
            self.next_piece = random.randrange(1, 8)

    def check_hit(self):
        """
        Check to see if the current tile in motion has collided with anything in the grid or the ground.
        If so, add square locations to grid and kill square.
        """
        hit = False
        for piece in reversed(self.piece_list):
            grid_loc_x = int(piece.center_x // SIZE)
            grid_loc_y = int(piece.center_y // SIZE)

            if piece.is_moving is False:
                self.grid[grid_loc_y][grid_loc_x] = color_dict[piece.file_name]
                if piece.frame_count < 10:
                    self.game_over = True
                piece.kill()
                self.has_switched = False

        for piece in self.piece_list:
            grid_loc_x = int(piece.center_x // SIZE)
            grid_loc_y = int(piece.center_y // SIZE)
            if self.grid[grid_loc_y - 1][grid_loc_x] != 0 and piece.is_moving is True:
                for piece in self.piece_list:
                    piece.is_moving = False
                hit = True
            elif piece.bottom <= 0 and piece.is_moving is True:
                grid_loc_x = int(piece.center_x // SIZE)
                grid_loc_y = int(piece.center_y // SIZE)
                for piece in self.piece_list:
                    piece.is_moving = False
                hit = True

        if hit:
            arcade.play_sound(self.thud)

    def update(self, delta_time):
        if self.game_over is False:
            self.frame_count += 1
            self.piece_list.update()
            self.check_hit()
            self.check_clear_row()

            # If no pieces are moving (a.k.a no shape is movable) create a new shape.
            moving_pieces = False
            for piece in self.piece_list:
                if piece.is_moving:
                    moving_pieces = True
            if moving_pieces is False:
                self.new_shape()

            # Workaround for bug where the first piece after changing to a new level would result in that piece having previous level velocity.
            for piece in self.piece_list:
                if piece.level != self.level:
                    piece.level = self.level
                    piece.drop_speed = int(30 / (1 + (self.level * 0.1)))

    def on_key_press(self, key, modifiers):
        """
        If there are no obstructions or walls preventing movement, move or rotate.
        If there is a border in the way, the piece is moved to allow rotation.
        """
        if self.game_over is False:

            # Left/A key, move left
            if key == arcade.key.LEFT or key == arcade.key.A:
                can_move = True
                for piece in self.piece_list:
                    if piece.is_moving:
                        grid_loc_x = int(piece.center_x // SIZE)
                        grid_loc_y = int(piece.center_y // SIZE)
                        try:
                            if (
                                piece.center_x - SIZE < 0
                                or self.grid[grid_loc_y][grid_loc_x - 1] != 0
                            ):
                                can_move = False
                        # Should never get here, only for consistency
                        except IndexError:
                            can_move = False
                if can_move:
                    for piece in self.piece_list:
                        piece.change_x = -SIZE

            # Right/D key, move right
            elif key == arcade.key.RIGHT or key == arcade.key.D:
                can_move = True
                for piece in self.piece_list:
                    if piece.is_moving:
                        grid_loc_x = int(piece.center_x // SIZE)
                        grid_loc_y = int(piece.center_y // SIZE)
                        try:
                            if (
                                piece.center_x + SIZE > GAME_WIDTH
                                or self.grid[grid_loc_y][grid_loc_x + 1] != 0
                            ):
                                can_move = False
                        except IndexError:
                            can_move = False
                if can_move:
                    for piece in self.piece_list:
                        piece.change_x = SIZE

            # UP/W key, rotate clockwise. If against a wall, will try to move to the side and rotate.
            elif key == arcade.key.UP or key == arcade.key.W:
                can_move = True
                can_move_left = True
                can_move_right = True
                for piece in self.piece_list:
                    if piece.is_moving:
                        grid_loc_x = int(piece.center_x // SIZE)
                        grid_loc_y = int(piece.center_y // SIZE)
                        try:
                            test_x, test_y = arcade.rotate_point(
                                piece.center_x,
                                piece.center_y,
                                piece.rotate_point_x,
                                piece.rotate_point_y,
                                270,
                            )
                            test_x = int(test_x // SIZE)
                            test_y = int(test_y // SIZE)
                            if test_x < 0 or test_y < 0:
                                can_move_left = False
                            if self.grid[test_y][test_x] != 0:
                                can_move = False
                        except IndexError:
                            can_move_right = False

                if can_move and can_move_left and can_move_right:
                    for piece in self.piece_list:
                        piece.center_x, piece.center_y = arcade.rotate_point(
                            piece.center_x,
                            piece.center_y,
                            piece.rotate_point_x,
                            piece.rotate_point_y,
                            270,
                        )

                # If against the wall, move the piece away from the wall and rotate
                elif can_move_left is False:
                    self.on_key_press(arcade.key.RIGHT, 0)
                    self.piece_list.update()
                    self.on_key_press(arcade.key.UP, 0)
                elif can_move_right is False:
                    self.on_key_press(arcade.key.LEFT, 0)
                    self.piece_list.update()
                    self.on_key_press(arcade.key.UP, 0)

            # Allows the active pieces to go down faster.
            elif key == arcade.key.DOWN or key == arcade.key.S:
                for piece in self.piece_list:
                    piece.drop_speed = 2

            # Allows piece to be held for future use, but can only be switched once per piece.
            elif key == arcade.key.Q:
                if self.has_switched is False:
                    if self.piece_held == -1:
                        self.piece_held = color_dict[self.piece_list[0].file_name]
                        for piece in reversed(self.piece_list):
                            piece.kill()
                        self.new_shape()
                    else:
                        temp = color_dict[self.piece_list[0].file_name]
                        for piece in reversed(self.piece_list):
                            piece.kill()
                        self.new_shape(self.piece_held)
                        self.piece_held = temp
                    self.has_switched = True

        # Interacting when game is not active
        if self.game_over:
            if key == arcade.key.SPACE:
                self.piece_list = arcade.SpriteList()
                self.new_grid()
                self.game_over = False
                self.level = 0
                self.score = 0
                self.piece_held = None
            if key == arcade.key.Q and self.frame_count:
                self.take_highscore()

    def on_key_release(self, key, modifiers):
        # Resets down velocity when key is released.
        if key == arcade.key.DOWN or arcade.key.S:
            for piece in self.piece_list:
                piece.drop_speed = int(30 / (1 + (self.level * 0.1)))

    def take_highscore(self):
        """
        Allows user to save their score to a local scoreboard.
        """
        try:
            name = ""
            while len(name) < 3:
                name = easygui.enterbox(msg="Enter Initials (3):")
            name = name[:3]
            name = name.upper()
            to_write = str(self.score) + " " + name + "\n"
            hs = open("hs.txt", "r+")
            contents = hs.readlines()
            contents.append(to_write)
            contents = sorted(contents, reverse=True)
            hs.seek(0)
            hs.truncate()
            for item in contents:
                hs.write(str(item))
            hs.close()
            print(contents)

        except:
            print("User hit ESC, no write to hs.txt")


def main():
    """ Main method """

    window = Tetris(SCREEN_WIDTH, SCREEN_HEIGHT)
    window.setup()
    arcade.run()


main()
